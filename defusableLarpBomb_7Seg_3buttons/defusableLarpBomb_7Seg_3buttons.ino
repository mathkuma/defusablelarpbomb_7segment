/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <technoLARP@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return. Marcelin
 * ----------------------------------------------------------------------------
 */

/*
 * ----------------------------------------------------------------------------
 * This is a defusable bomb/timer intended for larp (live action role playing) game 
 * and/or airsoft game. !! THIS IS NOT A REAL BOMB !! 
 * 
 * You need an arduino uno or nano board
 * a 7segment*4 display with TM1637 controller
 * a 5v piezo buzzer
 * a red LED with an appropriate resistor (330 ohms for exemple)
 * 3* momentary on/off switches, normally open
 * 4 wires and connectors
 * ----------------------------------------------------------------------------
 */

#include <Arduino.h>
#include <TM1637Display.h>    // library can be found here https://github.com/avishorp/TM1637

// define initial remain minutes ans seconds
#define DEFAULTMINUTES 5
#define DEFAULTSECONDS 0

// define connection pins
// TM1637 connection pins
#define CLK_PIN 7
#define DIO_PIN 8

// various pins
#define REDLED_PIN 5
#define BUZZER_PIN 6
#define DOWNBUTTON_PIN 9
#define UPBUTTON_PIN 10
#define ACTIVATIONBUTTON_PIN 11

// define the bomb status
enum {BOMB_POWERON, BOMB_ACTIVATED, BOMB_DETONATED, BOMB_SAFE};
byte bombStatus = BOMB_POWERON;

// define wires
#define WIRES_NB 4
byte pinWire[WIRES_NB] = {A2, A3, 12, 13};
enum {WIRE_EXPLOSION, WIRE_SAFE, WIRE_DELAY, WIRE_NEUTRAL, WIRE_RANDOM, WIRE_CUT};

// define an action for each wire
byte actionWire[WIRES_NB] = {
  WIRE_RANDOM,       // wire 1 connected on pin A2
  WIRE_RANDOM,       // wire 2 connected on pin A3 
  WIRE_RANDOM,       // wire 3 connected on pin 12 
  WIRE_RANDOM        // wire 4 connected on pin 13
  };

// random wires
byte randomWireExplosion = 1;
byte randomWireSafe = 1;
byte randomWireDelay = 2;

byte randomWireAssignation[WIRES_NB] = {WIRE_NEUTRAL, WIRE_NEUTRAL, WIRE_NEUTRAL, WIRE_NEUTRAL};


// init the 4*7seg display
TM1637Display display(CLK_PIN, DIO_PIN);

// remaining time
int remainMinutes;
int remainSeconds;

// various
bool oneTimeAnimation = true;
bool statusLed;

// time reference
unsigned long int ul_PreviousMillis;
unsigned long int ul_CurrentMillis;
unsigned long int ul_Interval;

unsigned long int ul_PreviousMillisDot;
unsigned long int ul_CurrentMillisDot;
unsigned long int ul_IntervalDot;

// clock points
bool clockPoints = true;

// display
bool displayDirty = true;

enum {ANIM_EMPTY, ANIM_EXPLODED, ANIM_SAFE, ANIM_BOOM};


/************************************/
/*** SETUP                        ***/
/************************************/
void setup() 
{
  // configure 7seg display
  display.setBrightness(0x0f);
 
  // configure the activation button
  pinMode(ACTIVATIONBUTTON_PIN, INPUT);
  digitalWrite(ACTIVATIONBUTTON_PIN, HIGH);

  // configure the up button
  pinMode(UPBUTTON_PIN, INPUT);
  digitalWrite(UPBUTTON_PIN, HIGH);

  // configure the down button
  pinMode(DOWNBUTTON_PIN, INPUT);
  digitalWrite(DOWNBUTTON_PIN, HIGH);

  // configure the red led
  statusLed = LOW;
  pinMode(REDLED_PIN, OUTPUT);
  digitalWrite(REDLED_PIN, statusLed);

  // configure 4 wires
  for (int i=0;i<WIRES_NB;i++)
  {
    pinMode(pinWire[i], INPUT);
    digitalWrite(pinWire[i], HIGH);
  }

  // default remain time
  remainMinutes = DEFAULTMINUTES;
  remainSeconds = DEFAULTSECONDS;

  oneTimeAnimation = true;

  // beep one time
  shortBeep();

  // init random
  randomSeed(analogRead(0));

  // various time reference
  ul_PreviousMillis = millis();
  ul_CurrentMillis = ul_PreviousMillis;
  ul_Interval = 200;

  ul_PreviousMillisDot = millis();
  ul_CurrentMillisDot = ul_PreviousMillis;
  ul_IntervalDot = 500;

  
}
/************************************/
/*** END OF SETUP                 ***/
/************************************/




/************************************/
/*** LOOP                         ***/
/************************************/
void loop() 
{
  // loop regarding the current bomb status
  switch (bombStatus) 
  {
    case BOMB_POWERON:
      // setup remain time
      powerOn();
    break;

    case BOMB_ACTIVATED:
      // the bomb is activate
      activated();
    break;
    
    case BOMB_DETONATED:
      // detonation animation
      detonated();
    break;

    case BOMB_SAFE:
      // detonation animation
      safe();
    break;
    
    default:
      // do nothing      
    break;
  }
}
/************************************/
/*** END OF LOOP                  ***/
/************************************/





/************************************/
/*** ADDITIONNAL FUNCTIONS        ***/
/************************************/

// The bomb is not yet active, waiting for time confirmation
void powerOn()
{
  // blink the red led
  ul_CurrentMillis = millis();
  if(ul_CurrentMillis - ul_PreviousMillis > ul_Interval)
  {
    ul_PreviousMillis = ul_CurrentMillis;

    // turn on/off the red led
    statusLed = !statusLed;
    digitalWrite(REDLED_PIN, statusLed);
  }

  // check if time changed
  // UPBUTTON_PIN pressed, increase time
  if (pressedButton(UPBUTTON_PIN))
  {
    remainMinutes++;
    remainMinutes=min(60, remainMinutes);

    displayDirty=true;
  }

  // DOWNBUTTON_PIN pressed, decrease time
  if (pressedButton(DOWNBUTTON_PIN))
  {
    remainMinutes--;
    remainMinutes=max(0, remainMinutes);

    displayDirty=true;
  }

  // check if bomb has been activated
  // ACTIVATIONBUTTON_PIN pressed, activate the bomb
  if (pressedButton(ACTIVATIONBUTTON_PIN))
  {
    //bombIsActive = true;
    bombStatus = BOMB_ACTIVATED;

    // bomb is now active
    // beep 2 times
    doubleBeep();
  
    // turn on red led
    digitalWrite(REDLED_PIN, HIGH);

    // manage random wires
    manageRandomWires();
  
    // change delay to 1000 ms
    ul_Interval = 1000;
    ul_PreviousMillis = millis();
  }

  // update 7seg display to display remain time
  updateDisplay();
}

// the bomb is active
void activated()
{
  // check time
  if ( (remainSeconds == 0) && (remainMinutes == 0) )
  {
    // time's up !!
    bombStatus = BOMB_DETONATED;
  }
  else
  {
    // some time remains
    // wait 1 s
    ul_CurrentMillis = millis();
    if(ul_CurrentMillis - ul_PreviousMillis > ul_Interval)
    {
      ul_PreviousMillis = ul_CurrentMillis;
  
      // decrease remaining time
      remainSeconds--;
      
      // manage changing minutes
      if (remainSeconds == -1)
      {
        remainSeconds = 59;
        remainMinutes--;
      }
  
      // beep every 30 s
      if ( (remainSeconds == 0) || (remainSeconds == 30) )
      {
        shortBeep();
      }
  
      // beep every seconds in the last 10 seconds 
      if ( (remainMinutes == 0) && (remainSeconds <= 10) )
      {
        shortBeep();
      }
    
      displayDirty=true;
    }

    // check if wires are cut
    manageWires();
  }

  // check time to change : on or off
  ul_CurrentMillisDot = millis();
  if(ul_CurrentMillisDot - ul_PreviousMillisDot > ul_IntervalDot)
  {
    ul_PreviousMillisDot = ul_CurrentMillisDot;
    clockPoints = !clockPoints;

    displayDirty=true;
  }

  // update 7seg display to display remain time
  updateDisplay();
}


// the bomb detonate or have detonated
void detonated()
{
  
  
  // show exploding animation one time
  if (oneTimeAnimation)
  {
    oneTimeAnimation = false;
    
    // beep 
    longBeep();

    // turn off the led led
    digitalWrite(REDLED_PIN, LOW);

    for (int i=0;i<15;i++)
    {
      //display.setSegments(SEG_EMPTY, 4, 0);
      showAnimation(ANIM_BOOM);
      delay(200);
      //display.setSegments(SEG_BOOM, 4, 0);
      showAnimation(ANIM_EMPTY);
      delay(200);
    }
  }
  else
  {
    // the bomb already exploded
    // just show a "- - - -" on display
    showAnimation(ANIM_EXPLODED);
    delay(1000);
  }
}



// the bomb is safe
void safe()
{
  // show safe animation one time
  if (oneTimeAnimation)
  {
    oneTimeAnimation = false;
    
    // beep 
    shortBeep();

    // turn off the led led
    digitalWrite(REDLED_PIN, LOW);
    
    for (int i=0;i<15;i++)
    {
      //display.setSegments(SEG_SAFE, 4, 0);
      showAnimation(ANIM_SAFE);
      delay(200);
      //display.setSegments(SEG_EMPTY, 4, 0);
      showAnimation(ANIM_EMPTY);
      delay(200);
    }
  }
  else
  {
    // the bomb already safe
    showAnimation(ANIM_SAFE);
  }
}

void showAnimation(byte toShow)
{
  //define display segments
  uint8_t SEG_EMPTY[] = {0x00, 0x00, 0x00, 0x00};
  uint8_t SEG_BOOM[] = {0x7C, 0x5C, 0x5C, 0x54};       // b, o, o, n
  uint8_t SEG_EXPLODED[] = {SEG_G, SEG_G, SEG_G, SEG_G};  // -, -, -, -
  uint8_t SEG_SAFE[] = {0x6D, 0x77, 0x71, 0x79};      //S, A, F, E

  switch (toShow) 
  {
    case ANIM_EMPTY:
      display.setSegments(SEG_EMPTY, 4, 0);
    break;

    case ANIM_EXPLODED:
      display.setSegments(SEG_EXPLODED, 4, 0);
    break;
    
    case ANIM_SAFE:
      display.setSegments(SEG_SAFE, 4, 0);
    break;

    case ANIM_BOOM:
      display.setSegments(SEG_BOOM, 4, 0);
    break;
    
    default:
      // do nothing      
    break;
  }
}



// manage a momentary on/off button
bool pressedButton(int b)
{
  if (digitalRead(b)==false)
  {
    delay(50);
    while(digitalRead(b)==false)
    {
      // debounce delay
      delay(50);
    }
    return true;
  }
  else
  {
    return false;
  }
}





// manage cutted wires
void manageWires()
{
  // scan all wires
  for (int i=0;i<WIRES_NB;i++)
  {
    // if the i wire was not previously cutted ans is now cut
	if ( (actionWire[i] != WIRE_CUT) && (digitalRead(pinWire[i]) == HIGH) )
    { 
      switch (actionWire[i]) 
      {
        case WIRE_NEUTRAL:
          // wire is neutral, nothing to do
          actionWire[i]=WIRE_CUT;
        break;
        
        case WIRE_DELAY:
          // divide ul_Interval by 2
          doubleBeep();
          ul_Interval/=2;
          actionWire[i]=WIRE_CUT;
        break;
    
        case WIRE_SAFE:
          // if bomb status already changed to BOMB_DETONATED, don't change to BOMB_SAFE
          if (bombStatus != BOMB_DETONATED)
          {
            // bomb is now safe
            shortBeep();
            bombStatus = BOMB_SAFE;
          }
          actionWire[i]=WIRE_CUT;
        break;
        
        case WIRE_EXPLOSION:
          // detonate the bomb
          bombStatus = BOMB_DETONATED;
          actionWire[i]=WIRE_CUT;
        break;
        
        default:
          // do nothing      
        break;
      }
    }    
  }
}



// random wires choice
void manageRandomWires()
{
  int randomWireCount = 0;
  int randomWireCountCopy = 0;
  int indexRandomWireAssignation = 0;
  for (int i=0;i<WIRES_NB;i++)
  {
    if (actionWire[i]==WIRE_RANDOM)
    {
      randomWireCount++;
    }
  }

  randomWireCountCopy=randomWireCount;
  
  // prepare WIRE_EXPLOSION wires
  while( (randomWireCount>0) && (randomWireExplosion>0) )
  {
    randomWireAssignation[indexRandomWireAssignation]=WIRE_EXPLOSION;
    indexRandomWireAssignation++;
    randomWireExplosion--;
    randomWireCount--;
  }

  // prepare WIRE_SAFE wires
  while( (randomWireCount>0) && (randomWireSafe>0) )
  {
    randomWireAssignation[indexRandomWireAssignation]=WIRE_SAFE;
    indexRandomWireAssignation++;
    randomWireSafe--;
    randomWireCount--;
  }

  // prepare WIRE_DELAY wires
  while( (randomWireCount>0) && (randomWireDelay>0) )
  {
    randomWireAssignation[indexRandomWireAssignation]=WIRE_DELAY;
    indexRandomWireAssignation++;
    randomWireDelay--;
    randomWireCount--;
  }

  // create an array of random number between 10 and 50
  int randomArray[WIRES_NB];
  for (int i=0;i<WIRES_NB;i++)
  {
    randomArray[i]=random(10,50);
  }
    
  // assign new wire in actionWire[]  
  for (int i=0;i<WIRES_NB;i++)
  {
    if (actionWire[i]==WIRE_RANDOM)
    {
      // find the greater index in randomArray
      int indexToUse = indexOfMaxValue(randomWireCountCopy, randomArray);
      randomArray[indexToUse]=0;
      
	  // assign the new wire type with the random index
      actionWire[i]=randomWireAssignation[indexToUse];
    }
  }
}


// index of max value in an array
int indexOfMaxValue(int arraySize, int arrayToSearch[])
{
  int indexMax=0;
  int currentMax=0;
  
  for (int i=0;i<arraySize;i++)
  {
    if (arrayToSearch[i]>=currentMax)
    {
      currentMax = arrayToSearch[i];
      indexMax = i;
    }
  }
  
  return(indexMax);
}

// various beep function to sound the buzzer
void shortBeep() 
{
  beep(1000, 50);
}

void doubleBeep()
{
  beep(1000, 50);
  delay(200);
  beep(1000, 50);
}
 
void longBeep() 
{
  beep(1000, 5000);
}


void beep(int freqBeep, int delayBeep)
{
  tone(BUZZER_PIN, freqBeep, delayBeep);
}


// display function
void updateDisplay()
{
  if (displayDirty)
  {
    displayDirty=false;

    int displayTime = remainMinutes*100 + remainSeconds;
    
    // update 7seg display to display new time
    if (clockPoints)
    {
      //  with clockpoints
      display.showNumberDecEx(displayTime, 0b01000000, true, 4, 0);
    }
    else
    {
      //  without clockpoints
      display.showNumberDecEx(displayTime, 0b00000000, true, 4, 0);
    }
  }
}
/************************************/
/*** END OF ADDITIONNAL FUNCTIONS ***/
/************************************/
