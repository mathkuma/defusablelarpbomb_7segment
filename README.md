# DefusableLarpBomb_7segment

A defusable bomb simulator with 4 actives wires, running on arduino with a 4*7segment display & TM1637 driver  
This defusable bomb/timer is intended for larp (live action role playing) game and/or airsoft game.  
This bomb simulator is an upgrade of the [simpleLarpBomb](https://gitlab.com/mathkuma/simplelarpbomb_7segment)   
!! THIS IS NOT A REAL BOMB !! 


## User manual
### Plant the bomb
1. make sure the 4 wires are connected to ground
1. power on the bomb
1. press UP button to increase the count down timer
1. press DOWN button to decrease the count down timer
1. press ACTIVATION button to activate & start the bomb

When time expire, the bomb produce a long beep noise and show a blinking "b o o n" on display . After 5 seconds, the bomb show a "- - - -". You can power it off & on to reuse it again.  

## Set-up the defuse wires
###  Manually
You can set up the defuse wires manually in the arduino code. Find the __actionWire[WIRES_NB]__ array and choose an action for each wire.  

Wire type  |  Action
---------|------------
WIRE_SAFE | Change bomb status to safe and defuse the bomb
WIRE_EXPLOSION | Change bomb status to EXPLODED and detonate the bomb
WIRE_DELAY | Divide delay by 2, the count down timer is faster
WIRE_NEUTRAL | Do nothing


If you cut or unplug a WIRE_SAFE wire, the bomb status change to safe and a blinking " S A F E " message is shown on display. After 5 seconds, the " S A F E " message stop to blink. You can power the bomb off & on to reuse it again (make sure to reconnect all wires !).  
If you cut or unplug a WIRE_EXPLOSION wire, the bomb status change to exploded. In this case, the bomb act as the time is up and show a blinking "b o o n" message during 5 seconds.

__Exemple 1__
The wire connected on __pin A2__ will defuse the bomb, the wire connected on __pin A3__ will detonate the bomb, ...
```c
// define an action for each wire
byte actionWire[WIRES_NB] = {
  WIRE_SAFE,        // wire 1 connected on pin A2
  WIRE_EXPLOSION,   // wire 2 connected on pin A3 
  WIRE_DELAY,       // wire 3 connected on pin 12 
  WIRE_NEUTRAL      // wire 4 connected on pin 13
  };
```
###  Randomly
You can set up the defuse wires randomly. Find the __actionWire[WIRES_NB]__ array and use __WIRE_RANDOM__ as action. 
At startup, arduino code will choose an action to each random wire in function ```void manageRandomWires()```. This code will try to meet the requirements given in the following variables and affect as many as possible wires, in this order (all __randomWireExplosion__ first, __randomWireSafe__ then and finally __randomWireDelay__ wires). 
If any random wires left, WIRE_NEUTRAL will be use. 


__Exemple 2__
The 4 wires will be chosen randomly. 1 will detonate the bomb, 1 wil defuse the bomb, the 2 others will accelerate the count down timer.
```c
// define an action for each wire
byte actionWire[WIRES_NB] = {
  WIRE_RANDOM,       // wire 1 connected on pin A2
  WIRE_RANDOM,       // wire 2 connected on pin A3 
  WIRE_RANDOM,       // wire 3 connected on pin 12 
  WIRE_RANDOM        // wire 4 connected on pin 13
  };

// random wires
byte randomWireExplosion = 1;
byte randomWireSafe = 1;
byte randomWireDelay = 2;
```
### Mix random & manually chosen wires
You can mix the random wires and the manually chosen wires. Only the __WIRE_RANDOM__ wires will be replace by random action.

__Exemple 3__
In the following exemple, the wire 3 connected on __pin 12__ will detonate the bomb. 1 other will also detonate the bomb, 1 will defuse the bomb and the last one will accelerate the the count down timer
```c
// define an action for each wire
byte actionWire[WIRES_NB] = {
  WIRE_RANDOM,       // wire 1 connected on pin A2
  WIRE_RANDOM,       // wire 2 connected on pin A3 
  WIRE_EXPLOSION,    // wire 3 connected on pin 12 
  WIRE_RANDOM        // wire 4 connected on pin 13
  };

// random wires
byte randomWireExplosion = 1;
byte randomWireSafe = 1;
byte randomWireDelay = 2;
```



## Hardware
- an arduino uno or arduino nano board
- a 7segment*4 display with TM1637 controller with a : between the segments.    [This one will work fine](https://robotdyn.com/4-digit-led-display-tube-7-segments-tm1637-50x19mm-red-double-dots-clock.html)
- a 5v piezo buzzer
- a 5v red LED with an appropriate resistor (330 ohms for exemple)
- 3* momentary on/off button, normally open

## Library
You need the **tm1637.h** library to manage the 4* 7segment display. 
This library is available via the Arduino IDE library manager. you can simply use the menu:
Sketch->Include Library->Manage Libraries... Then search for __tm1637 by Avishay Orpaz__. 
This library is also available on [github](https://github.com/avishorp/TM1637)  



## Some exemples
<img src="./images/defusableLarpBomb_7segment_exemple1.png" width="400"><br>
Homemade dev board with an arduino nano (the white JST connector is useless)  

<img src="./images/defusableLarpBomb_7segment_exemple2.png" width="400"><br>
A full bomb made of an arduino uno and a [seedstudio tick tock shield](https://www.seeedstudio.com/Starter-Shield-EN-Tick-Tock-shield-v2-p-2768.html)   


<img src="./images/defusableLarpBomb_7segment_exemple3.png" width="400"><br>
Another complete exemple, used during escape game. The 4 wires are on the left, connected to ground with a banana plug.  
Wires attach to the transparent pmma pannel are uselless & decorative.    
arduino uno + [seedstudio tick tock shield](https://www.seeedstudio.com/Starter-Shield-EN-Tick-Tock-shield-v2-p-2768.html)   
This enclosure is made of mdf, transparent pmma & lasercut. 

 
## Wiring
Arduino  |  Component
---------|------------
D5 | resistor & red LED +
D6 | buzzer +
D7 | 7 segment display pin CLK
D8 | 7 segment display pin DIO
D9 | down button
D10 | up button
D11 | activation button
A2 | wire 1
A3 | wire 2
D12 | wire 3
D13 | wire 4

## Schematic
![](./images/defusableLarpBomb_7segment_schematic.png)

ST_GND & ST_WIRES are 2 screw terminals block (4 pins each). Just add electric wires between the 4 terminals. You can use any other connectors type, like dupont header or banana plug (see exemples).

## Powering
There are 4 ways to powering the arduino uno board
[Some explanations here](https://www.open-electronics.org/the-power-of-arduino-this-unknown/)
1. USB plug with 5v
1. DC 2.1mm plug with 6-12v
1. 5v pin with 5v
1. Vin pin with 6-12v
